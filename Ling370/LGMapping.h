//
//  LGMapping.h
//  Ling370
//
//  Created by Nolan Astrein on 3/26/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGMapping : NSObject

+ (LGMapping *)sharedInstance;

- (void)startTraining;
- (void)endTraining;

- (void)processDocument:(NSString *)document forCategory:(NSString *)category;
- (NSDictionary *)resultsForDocument:(NSString *)document;

@end
