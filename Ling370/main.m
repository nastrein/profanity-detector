//
//  main.m
//  Ling370
//
//  Created by Nolan Astrein on 3/26/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
