//
//  LGAppDelegate.m
//  Ling370
//
//  Created by Nolan Astrein on 3/26/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "LGAppDelegate.h"
#import "LGMapping.h"

static const int kNumFiles = 2;
static const NSString *kScriptPath =  @"/Users/nastrein/Desktop/Ling370/Ling_Data/webscraper.py";
static const NSString *kGoodURLs = @"/Users/nastrein/Desktop/Ling370/Ling_Data/urls/good.txt";
static const NSString *kBadURLs = @"/Users/nastrein/Desktop/Ling370/Ling_Data/urls/bad.txt";
static const NSString *kGoodDataPath = @"/Users/nastrein/Desktop/Ling370/Ling_Data/good_data/good_data";
static const NSString *kBadDataPath = @"/Users/nastrein/Desktop/Ling370/Ling_Data/bad_data/bad_data";

@interface LGAppDelegate ()

@end

@implementation LGAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    [self scrapeDataFromSource:kGoodURLs toDestination:kGoodDataPath];
    [self scrapeDataFromSource:kBadURLs toDestination:kBadDataPath];
   // [self loadMap];
   // [self analyzeDocument:@"god"];
}

- (void)loadMap
{
    [[LGMapping sharedInstance] startTraining];
    
    for(int i = 0; i < kNumFiles; ++i) {
        NSError *error = nil;
        NSString *path = [NSString stringWithFormat:@"/Users/nastrein/Desktop/Ling370/Ling_Data/good_data/good_data%d.txt",i];
        NSString *document = [NSString stringWithContentsOfFile:path
                                                       encoding:NSUTF8StringEncoding
                                                          error:&error];
        [[LGMapping sharedInstance] processDocument:document forCategory:@"good"];
    }
    
    for(int i = 0; i < kNumFiles; ++i) {
        NSError *error = nil;
        NSString *path = [NSString stringWithFormat:@"/Users/nastrein/Desktop/Ling370/Ling_Data/bad_data/bad_data%d.txt",i];
        NSString *document = [NSString stringWithContentsOfFile:path
                                                       encoding:NSUTF8StringEncoding
                                                          error:&error];
        [[LGMapping sharedInstance] processDocument:document forCategory:@"bad"];
    }
    
    [[LGMapping sharedInstance] endTraining];
}

- (void)analyzeDocument:(NSString *)document
{
    NSDictionary * results = [[LGMapping sharedInstance] resultsForDocument:document];
    
    for (id key in results) {
        NSLog(@"key: %@, value: %@ \n", key, [results objectForKey:key]);
    }
}

- (void)scrapeDataFromSource:(const NSString *)source toDestination:(const NSString *)destination
{
    NSTask* task = [[NSTask alloc] init];
    [task setLaunchPath:@"/usr/local/bin/python"];
    [task setArguments:[NSArray arrayWithObjects:kScriptPath, source, destination, nil]];
    
    [task setStandardInput:[NSPipe pipe]];
    
    NSPipe *stdOutPipe = nil;
    stdOutPipe = [NSPipe pipe];
    [task setStandardOutput:stdOutPipe];
    
    NSPipe* stdErrPipe = nil;
    stdErrPipe = [NSPipe pipe];
    [task setStandardError: stdErrPipe];
    
    [task launch];
    [task waitUntilExit];
    
    NSInteger exitCode = task.terminationStatus;
    if (exitCode != 0) {
        NSLog(@"Error!");
    }
    
    NSData* data = [[stdOutPipe fileHandleForReading] readDataToEndOfFile];
    NSLog(@"%@",[[NSString alloc] initWithBytes: data.bytes length:data.length encoding: NSUTF8StringEncoding]);
    
    [task interrupt];
    task = nil;
}

@end
