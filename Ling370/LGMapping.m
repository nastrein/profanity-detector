//
//  LGMapping.m
//  Ling370
//
//  Created by Nolan Astrein on 3/26/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import "LGMapping.h"
#import <LatentSemanticMapping/LatentSemanticMapping.h>

@interface LGMapping ()

@property (strong, nonatomic) __attribute__((NSObject)) LSMMapRef LSMMap;
@property (strong, nonatomic) NSMutableDictionary *categories;
@property (strong, nonatomic) NSMutableDictionary *inverseCategories;

@end

@implementation LGMapping

static LGMapping *gSharedInstance = NULL;

+ (LGMapping *)sharedInstance
{
    static dispatch_once_t sOnceToken = 0;
    dispatch_once(&sOnceToken, ^{
        gSharedInstance = [[LGMapping alloc] init];
    });
    return(gSharedInstance);
}

- (id)init
{
    self = [super init];
    if(self) {            
			self.LSMMap = LSMMapCreate(kCFAllocatorDefault, kLSMMapPairs | kLSMMapTriplets);
			self.categories = [[NSMutableDictionary alloc] init];
            self.inverseCategories = [[NSMutableDictionary alloc] init];
			[self addCategory:@"bad"];
			[self addCategory:@"good"];
    }
    return self;
}

- (void)startTraining
{
	OSStatus status = LSMMapStartTraining(self.LSMMap);
	if (status != 0) {
		NSLog(@"LSMMapStartTraining: %d", status);
    }
}

- (void)endTraining
{
	OSStatus status = LSMMapCompile(self.LSMMap);
	if (status != 0) {
		NSLog(@"LSMMapCompile: %d", status);
    }
}

- (void)processDocument:(NSString *)document forCategory:(NSString *)category
{
	NSArray *categories = @[category];
    [self trainDocument:document categories:categories];
}

- (void)trainDocument:(NSString *)document categories:(NSArray *)categories
{
	if (document == NULL || [document isEqualToString:@""]) {
		NSLog(@"Document to train is empty.");
        return;
    }
    
	LSMTextRef text = LSMTextCreate(kCFAllocatorDefault, self.LSMMap);
    
	NSString *language = (__bridge_transfer NSString *)CFStringTokenizerCopyBestStringLanguage((__bridge CFStringRef)document, (CFRange){ .length = document.length });
	if (language != NULL) {
		CFLocaleRef localeRef = CFLocaleCreate(kCFAllocatorDefault, (__bridge CFStringRef)language);
		LSMTextAddWords(text, (__bridge CFStringRef)document, localeRef, 0);
		CFRelease(localeRef);
    }
	else {
		LSMTextAddWords(text, (__bridge CFStringRef)document, NULL, 0);
    }
    
	for (NSString *category in categories) {
		LSMCategory lsmCategory = [self.categories[category] intValue];
		OSStatus status = LSMMapAddText(self.LSMMap, text, lsmCategory);
		if (status != 0) {
			NSLog(@"%d", status);
        }
    }
	CFRelease(text);
}

- (NSDictionary *)resultsForDocument:(NSString *)document
{    
	NSMutableDictionary *resultsDictionary = [[NSMutableDictionary alloc] init];
    
	if (document.length > 0) {
		LSMTextRef textRef = LSMTextCreate(kCFAllocatorDefault, self.LSMMap);
		OSStatus status = LSMTextAddWords(textRef, (__bridge CFStringRef)document, NULL, 0);
		if (status != 0) {
			NSLog(@"LSMTextAddWords: %d", status);
        }
        
		LSMResultRef resultRef = LSMResultCreate(kCFAllocatorDefault, self.LSMMap, textRef, self.categories.count, 0);
		if (resultRef != NULL){
			for (int i = 0; i != LSMResultGetCount(resultRef); ++i){
				LSMCategory lsmCategory = LSMResultGetCategory(resultRef, i);
				float score = LSMResultGetScore(resultRef, i);
				resultsDictionary[self.inverseCategories[@(lsmCategory)]] = @(score);
            }
			CFRelease(resultRef);
        }
		CFRelease(textRef);
    }
	return resultsDictionary;
}

- (void)addCategory:(NSString *)categoryName
{
	LSMCategory lsmCategory = LSMMapAddCategory(self.LSMMap);
	self.categories[categoryName] = @(lsmCategory);
	self.inverseCategories[@(lsmCategory)] = categoryName;
}

@end
