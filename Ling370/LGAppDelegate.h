//
//  LGAppDelegate.h
//  Ling370
//
//  Created by Nolan Astrein on 3/26/13.
//  Copyright (c) 2013 Nolan Astrein. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface LGAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
