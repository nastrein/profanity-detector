#!/usr/local/bin/python

from bs4 import BeautifulSoup
import re
import requests
import json
import time
import sys

counter = 0

def parse(url):
	r = requests.get(url)
	data = BeautifulSoup(r.text, "lxml")
	document = ''

	content = data.find_all('p')
	for p in content:
		document += ('\n' + p.text);

	content = data.find_all('h1')
	for h1 in content:
		document += ('\n' + h1.text);

	content = data.find_all('a')
	for a in content:
		document += ('\n' + a.text);

	save(document)

def save(document):
	global counter
	dest_file = sys.argv[2] + str(counter) + ".txt"
	print "dest file: " + dest_file

	file_handle = open(dest_file, 'w')
	file_handle.write(document);
	counter += 1

def main():
	reload(sys)
	sys.setdefaultencoding("utf-8")
	source_file = sys.argv[1]
	print "souce file: " + source_file

	file_handle = open(source_file, 'r')
	
 	urls = file_handle.readlines()
	for url in urls:
		print url
		parse(url)

if __name__ == '__main__':
	main()
